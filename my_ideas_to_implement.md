kategorie zysków: (w zależności od kategori wyświetlają się inne pola do uzupełnienia, do wyboru za pomocą przycisków)
- wypłata --> zyski_wypłata
- przedmiot --> zyski_przedmiot (samochód, laptop, telefon, długopis)
- pozostałe --> zyski_pozostałe (może ktoś coś wymyśli)

zyski_wypłata:
- od kogo
- za co
- ile
- waluta (pole do wyboru + dodanie innej waluty jeżeli takiej jeszcze nie ma w bazie)
- dodatkowy opis (nie trzeba wypełniać)
- data otrzymania wypłaty (tylko dzień, czas otrzymania raczej małoistotny)
- data dodania wpisu do aplikacji (zrobić żeby dodawała się automatycznie razem z akceptacją wpisu)

zyski_przedmiot:
- od kogo
- za co
- co
- dodatkowy opis (nie trzeba wypełniać)
- data otrzymania przedmiotu (tylko dzień, czas otrzymania raczej małoistotny)
- data dodania wpisu do aplikacji (zrobić żeby dodawała się automatycznie razem z akceptacją wpisu)

zyski_pozostałe:
- od kogo
- za co
- co
- dodatkowy opis (nie trzeba wypełniać)
- data otrzymania x (tylko dzień, czas otrzymania raczej małoistotny)
- data dodania wpisu do aplikacji (zrobić żeby dodawała się automatycznie razem z akceptacją wpisu)

straty/wydatki:
- ile 
- kategoria na co
- podkategoria na co
- data wydatków(sam dzień, bez godziny)
- data dodania wpisu do aplikacji (zrobić żeby dodawała się automatycznie razem z akceptacją wpisu)

kategorie wydatków:
- rachunki (podkategorie)
- wydatki podstawowe(podkategorie)
- pozostałe (podkategorie)

rachunki (nazwa, kwota):
- woda
- prąd
- gaz
- czynsz
- ubezpieczenie
- internet
- telewizja
- śmieci
- możliwość dodania nowej podkategorii
- możliwośc usunięcia podkategorii

wydatki podstawowe (nazwa, kwota):
- zakupy jedzeniowe
- możliwość dodania nowej podkategorii
- możliwośc usunięcia podkategorii

pozostałe (nazwa, kwota):
- kino
- teatr
- paliwo
- edukacja
- koncert
- możliwość dodania nowej podkategorii
- możliwośc usunięcia podkategorii

TO DO:
data przychodów i wydatków nie może być w przyszłości tylko maksymalnie do dnia wpisu


NA PÓŹNIEJ
osoby towarzyszące:
- rodzina
- rodzeństwo
- dziedkowie
- przyjaciel
- kumpel
- kilku znajomych
- współpracownicy
- możliwość dodania nowej osoby
- możliwość usunięcia osoby

konto rodzinne(zawiera konta indywidualne członków rodziny z ich sumowanie i wglądem do całości)
konto indywidualne(zyski i starty jednej osoby)



