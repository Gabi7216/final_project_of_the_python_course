from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from .models import Gains_payment, Gains_item, Gains_other, Categories_expenses, Losses, Bills, Basic_expenses, Other_expenses


# Create your views here.


def index(request):
    return render(request, "gains_and_losses/index.html") 
    #HttpResponse("Witaj na stronie głównej aplikacji zysków i strat!")

@login_required()
def gains(request):
    return render(request, "gains_and_losses/gains.html") 

def test(request):
    return render(request, "gains_and_losses/login/test.html") 
#def all_gains(request):
#    gains_payment_list = Gains_payment.objects.all()
#    gains_item_list = Gains_item.objects.all()
#    gains_other_list = Gains_other.objects.all()
#    all_gains_list = [gains_payment_list,gains_item_list,gains_other_list]
#    context = {"al_gains_list": all_gains_list}
#    return render(request, "gains_and_losses/all_gains.html", context)
     
@login_required()
def gains_payment(request):
    gains_payment_list = Gains_payment.objects.all()
    context = {"gains_payment_list": gains_payment_list}
    return render(request, "gains_and_losses/gains_payment.html", context)

@login_required()
def gains_item(request):
    gains_item_list = Gains_item.objects.all()
    context = {"gains_item_list": gains_item_list}
    return render(request, "gains_and_losses/gains_item.html", context)
    
@login_required()
def gains_other(request):
    gains_other_list = Gains_other.objects.all()
    context = {"gains_other_list": gains_other_list}
    return render(request, "gains_and_losses/gains_other.html", context)

@login_required()    
def losses(request):
    losses_list = Losses.objects.all()
    context = {"losses_list": losses_list}
    return render(request, "gains_and_losses/losses.html", context)
    

