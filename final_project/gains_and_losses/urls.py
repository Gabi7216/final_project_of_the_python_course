from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'gains_and_losses'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    path('gains/', views.gains, name='gains'),
    #path('all_gains/', views.all_gains, name='all_gains'),
    path('gains_payment/', views.gains_payment, name='gains_payment'),
    path('gains_item/', views.gains_item, name='gains_item'),
    path('gains_other/', views.gains_other, name='gains_other'),
    path('losses/', views.losses, name='losses'),
    ]
    
