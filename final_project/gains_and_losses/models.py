from django.db import models
from django.shortcuts import render

# Create your models here.

class Gains_payment(models.Model):
    from_who = models.CharField(max_length=100)
    for_what = models.CharField(max_length=300)
    amount = models.FloatField()
    description = models.CharField(max_length=500, blank = True)
    recived_date = models.DateField('received date')
    add_date = models.DateTimeField(auto_now_add = True)
        
    
class Gains_item(models.Model):
    from_who = models.CharField(max_length=100)
    for_what = models.CharField(max_length=300)
    what = models.CharField(max_length=300)
    description = models.CharField(max_length=500, blank = True)
    recived_date = models.DateField('received date')
    add_date = models.DateTimeField(auto_now_add = True)

    
class Gains_other(models.Model):
    from_who = models.CharField(max_length=100)
    for_what = models.CharField(max_length=300)
    what = models.CharField(max_length=300)
    description = models.CharField(max_length=500, blank = True)
    recived_date = models.DateField('received date')
    add_date = models.DateTimeField(auto_now_add = True)


class Categories_expenses(models.Model):
    category_name = models.CharField(max_length=200)
    description = models.CharField(max_length=500, blank=True)
    
    def __str__(self):
        return self.category_name


class Bills(models.Model):
    categories_expenses = models.ForeignKey(Categories_expenses, on_delete=models.CASCADE, verbose_name="the related categories_expenses and bills")
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500, blank = True)
    
    def __str__(self):
        return self.name
    
class Basic_expenses(models.Model):
    categories_expenses = models.ForeignKey(Categories_expenses, on_delete=models.CASCADE, verbose_name="the related categories_expenses and basic_expenses")
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500, blank = True)
    
    def __str__(self):
        return self.name
    
    
class Other_expenses(models.Model):
    categories_expenses = models.ForeignKey(Categories_expenses, on_delete=models.CASCADE, verbose_name="the related categories_expenses and other_expenses")
    name = models.CharField(max_length=300)
    description = models.CharField(max_length=500, blank = True)
    
    def __str__(self):
        return self.name
    
    
class Losses(models.Model):
    categories_expenses = models.ForeignKey(Categories_expenses, on_delete=models.CASCADE, verbose_name="the related categories_expenses and losses")
    subcategories_expenses_bills = models.ForeignKey(Bills, on_delete=models.CASCADE, verbose_name="the related subcategory bills and losses", null = True, blank =True)
    subcategories_expenses_basic_expenses = models.ForeignKey(Basic_expenses, on_delete=models.CASCADE, verbose_name="the related subcategory basic_expenses and losses", null = True, blank = True)
    subcategories_expenses_other_expenses = models.ForeignKey(Other_expenses, on_delete=models.CASCADE, verbose_name="the related subcategory other_expenses and losses",  null = True, blank = True)
    how_much = models.FloatField()
    for_what = models.CharField(max_length=500)
    expense_date = models.DateField('expense date')
    add_date = models.DateTimeField(auto_now_add = True)
    
    def __str__(self):
        return self.for_what
    
   # def choose_subcategory(self):
   #     if self.categories_expanses.name == "bills":
   #         return render(self.subcategories_expenses_bills)
   #     elif self.categories_expanses.name == "basic_expenses":
   #         return render(self.subcategories_expenses_basic_expenses)
   #     else:
   #         return render(self.subcategories_expenses_other_expenses)
    
    
    
