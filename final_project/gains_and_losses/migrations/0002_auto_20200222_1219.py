# Generated by Django 3.0.3 on 2020-02-22 11:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gains_and_losses', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='losses',
            name='subcategories_expenses_basic_expenses',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='gains_and_losses.Basic_expenses', verbose_name='the related subcategory basic_expenses and losses'),
        ),
        migrations.AlterField(
            model_name='losses',
            name='subcategories_expenses_bills',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='gains_and_losses.Bills', verbose_name='the related subcategory bills and losses'),
        ),
        migrations.AlterField(
            model_name='losses',
            name='subcategories_expenses_other_expenses',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='gains_and_losses.Other_expenses', verbose_name='the related subcategory other_expenses and losses'),
        ),
    ]
