from django.contrib import admin
from .models import Gains_payment, Gains_item, Gains_other, Losses, Categories_expenses, Bills, Basic_expenses, Other_expenses

# Register your models here.

admin.site.register(Gains_payment)
admin.site.register(Gains_item)
admin.site.register(Gains_other)
admin.site.register(Losses)
admin.site.register(Categories_expenses)
admin.site.register(Bills)
admin.site.register(Basic_expenses)
admin.site.register(Other_expenses)

