from django.apps import AppConfig


class GainsAndLossesConfig(AppConfig):
    name = 'gains_and_losses'
