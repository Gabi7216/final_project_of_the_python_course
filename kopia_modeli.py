from django.db import models

# Create your models here.

class Gains(models.Model):
    from_who = models.CharField(max_length=100)
    for_what = models.CharField(max_length=300)
    description = models.CharField(max_length=500, blank = True)
    amount = models.IntegerField()
    recived_date = models.DateField('received date')
    add_date = models.DateTimeField(auto_now_add = True)


class Losses(models.Model):
    CATEGORIES = [
    ("Bills", "Bills"),
    ("Basic_expenses", "Basic_examples"),
    ("Other_expenses", "Other_expenses"),
    ]
    how_much = models.IntegerField()
    ctegories_expenses = models.CharField(max_length=20, choices=CATEGORIES, default="Other_expenses")
    description = models.CharField(max_length=500)
    expense_date = models.DateField('expense date')
    add_date = models.DateTimeField(auto_now_add = True)
    
    
class Subcategories(models.Model):
    losses = models.ForeignKey(Losses, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    
    
    
    
